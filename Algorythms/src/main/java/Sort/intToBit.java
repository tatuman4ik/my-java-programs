package Sort;

public class intToBit {
    static int checkSign(StringBuilder sb, int x) {
        if (x < 0) {
            sb.append(1);
            x += Integer.MAX_VALUE + 1;
        }
        else sb.append(0);
        return x;
    }

    static void intToBit1(StringBuilder sb, int x) {
        for (int i = 30; i >= 0; i--) {
            if (x / (int) Math.pow(2, i) > 0) {
                sb.append(1);
                x = x % (int) Math.pow(2, i);
            } else sb.append(0);
        }
    }

    static void intToBit(StringBuilder sb, int x) {
        for (int i = 0; i < 31; i++) {
            sb.append((x >> (30 - i)) & 1);
        }
    }

    static String func(int x) {
        StringBuilder sb = new StringBuilder();
        x = checkSign(sb, x);
        intToBit(sb, x);
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(func(10));
        System.out.println(func(-10));
        System.out.println(func(1));
        System.out.println(func(-1));
        System.out.println(func(0));
        System.out.println(func(Integer.MAX_VALUE));
        System.out.println(func(Integer.MIN_VALUE));
    }
}
