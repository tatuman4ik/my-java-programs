package Sort;

public class Sorting {
    public static void main(String[] args) {
        int a[] = {1,2,3,4,5};
        int b[] = {1,3,5,7,9};
        int c[] = new int[a.length + b.length];
        int indA = 0;
        int indB = 0;
        int counter = 0;
        while (indA < a.length && indB < b.length) {
            c[counter++] = (a[indA] < b[indB])
                    ? a[indA++]
                    : b[indB++];
        }
        while (indA < a.length) {
            c[counter++] = a[indA++];
        }
        while (indB < b.length) {
            c[counter++] = b[indB++];
        }
        for (int i = 0; i < c.length; i++) {
            System.out.print(c[i] + " ");
        }

    }
}
