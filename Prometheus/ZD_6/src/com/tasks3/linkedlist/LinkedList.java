package com.tasks3.linkedlist;

public class LinkedList {
    public Node head;
    public int counter = 0;
    /* Конструктор без аргументів */
    public LinkedList() {}

    /* Додати елемент в кінець списку */
    public void add (Integer data) {
        counter++;
        Node newNode = new Node();
        newNode.setData(data);
        if (counter==1) {
            head = newNode;
            return;
        }
        Node last = head;
        while (last.getNext() != null) {
            last = last.getNext();
        }
        last.setNext(newNode);
    }

    /* Отримати елемент по індексу, повертає null якщо такий елемент недоступний */
    public Integer get(int index) {
        if ((counter == 0)||(index>=counter)) {
            return null;
        }
        Node dataByIndex = head;
        for (int i = 0; i<=index; i++) {
            if (i==index) {
                return dataByIndex.getData();
            }
            dataByIndex = dataByIndex.getNext();
        }
        return 0;
    }

    /* Вилучення елементу за індексом, повертає true у разі успіху або false в іншому випадку */
    public boolean delete(int index) {
        if ((counter == 0)||(index>=counter)) {
            return false;
        }
        if (index==0) {
            head = head.getNext();
            counter--;
            return true;
        }
        Node preLast = head;
        for (int i = 1; i<index; i++) {
            preLast = preLast.getNext();
        }
        if ((index+1)==this.size()) {
            preLast.setNext(null);
            counter--;
            return true;
        }
        Node delete = preLast.getNext();
        preLast.setNext(delete.getNext());
        counter--;
        return true;
    }

    /*Поверта розмір списку: якщо елементів в списку нема то повертає 0 (нуль)*/
    public int size() {
        return counter;
    }
    public static void main(String[] args){
        LinkedList list = new LinkedList();
        list.add(100);
        list.add(200);
        list.add(300);
        list.add(400);
        list.add(500);
        System.out.println(list.get(3));
        list.delete(4);
        System.out.println(list.get(0));
    }
}