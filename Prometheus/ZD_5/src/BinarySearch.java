public class BinarySearch {

    public static void main(String[] args) {

        int data[] = { 3, 6, 10, 12 };
        int numberToFind = 12;

        // PUT YOUR CODE HERE
        int min = 0;
        int max = data.length-1;
        int mid = (min+max)/2;
        boolean find=false;
        do {
            if (data[mid]<numberToFind) {
                if ((min == mid)&(min!=max)) {
                    mid++;
                    min++;
                } else {
                    min = mid;
                    mid = (max + min) / 2;
                }
            }
            else {
                    max = mid;
                    mid = (max + min) / 2;
                }
            if (data[mid]==numberToFind) {
                System.out.println(mid);
                find=true;
                break;
            }
        } while (min!=max);
        if (!find) {
            System.out.println("-1");
        }
        // PUT YOUR CODE HERE
    }
}
