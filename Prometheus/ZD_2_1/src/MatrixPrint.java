import java.util.Scanner; // импорт сканера

public class MatrixPrint {

        //PUT YOUR CODE HERE
        public static int check(String enter) { //проверка введенных данных
            boolean entNum;
            do {
                Scanner scan = new Scanner(System.in);
                System.out.println(enter);
                if(scan.hasNextInt()){ //проверка на число
                    entNum=true;
                    int itCanBeRight=scan.nextInt();
                    if ((itCanBeRight>0)&(itCanBeRight<=8)){ //проверка на отрицательность
                        return (itCanBeRight);
                    } else {
                        System.out.println("Більше нуля та не більше ніж 8");
                        entNum=false;
                    }
                } else {
                    System.out.println("Число, будь ласка");
                    entNum=false;
                }
            }while (true);
        }
    public static void main(String[] args){
        int size=check("Введіть розмір матриці");

        for (int j=0;j<size;j++){
            for (int i=0;i<size;i++){
                if ((i==j)|(i+j==(size-1))){
                    System.out.print("*");
                }
                else {
                    System.out.print(j*(size)+i+1);
                }
                if (i==(size-1)){
                    System.out.println();
                }
                else {
                    System.out.print(" ");
                }

            }
        }
        //PUT YOUR CODE HERE
    }
}