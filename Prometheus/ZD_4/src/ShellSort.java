public class ShellSort {

    public static void main(String[] args) {
        int[] array = {12, 34, 54, 2, 3};
        int length = array.length;
        for (int gap = length / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < length; i++) {
                int num = array[i];
                int j;
                for (j = i; j >= gap && array[j - gap] > num; j -= gap) {
                    array[j] = array[j - gap];
                }
                array[j] = num;
            }
        }
        for (int i = 0; i < length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}

