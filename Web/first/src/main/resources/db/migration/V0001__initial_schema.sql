create table history
(
    id serial primary key,
    x  integer,
    y  integer,
    z  integer
);
