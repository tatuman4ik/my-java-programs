package sql;

public interface Identifiable {
    Integer id();
}
