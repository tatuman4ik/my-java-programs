package sql;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public record Auth() {

    private static final Cookie[] c0 = new Cookie[0];

    public static Optional<UUID> getUUIDFromDatabase(String username, String password) {
        try (Connection connection = Database.conn()) {
            String query = "SELECT uuid FROM auth WHERE username = ? AND password = ?";
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, username);
            stmt.setString(2, password);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                UUID uuid = resultSet.getObject("uuid", UUID.class);
                return Optional.ofNullable(uuid);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static Optional<UUID> tryGetCookie(Cookie[] cookies) {
        return Arrays.stream(cookies != null ? cookies : c0)
                .filter(c -> c.getName().equals("UID"))
                .findAny()
                .map(Cookie::getValue)
                .map(UUID::fromString);
    }
    public static Optional<UUID> tryGetCookie(HttpServletRequest rq) {
        return tryGetCookie(rq.getCookies());
    }

    public static void registerUser(String username, String password) {
        try (Connection connection = Database.conn()) {
            String query = "INSERT INTO auth (username, password, uuid) VALUES (?, ?, ?)";
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, username);
            stmt.setString(2, password);
            UUID uuid = UUID.randomUUID();
            stmt.setObject(3, uuid);

            int rowsInserted = stmt.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("User registered successfully. UUID: " + uuid);
            } else {
                System.out.println("Failed to register user.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static UUID getCookieOrThrow(HttpServletRequest req) {
        return tryGetCookie(req)
                .orElseThrow(() -> new RuntimeException("Cookie is absent"));
    }

}
