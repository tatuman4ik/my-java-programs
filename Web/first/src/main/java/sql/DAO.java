package sql;

import java.sql.SQLException;
import java.util.Optional;

public interface DAO<T extends Identifiable> {

    void delete(int id) throws SQLException;
    void save(T t) throws SQLException;
    Optional<T> load(int id) throws SQLException;
}
