package sql;

import java.sql.ResultSet;
import java.util.LinkedList;

public class DbUtils {
    public static <A> Iterable<A> convert(ResultSet rs, FunctionEx<ResultSet, A> f) throws Exception {
        LinkedList<A> as = new LinkedList<>();
        while (rs.next()) {
            as.add(f.apply(rs));
        }
        return as;
    }
}
