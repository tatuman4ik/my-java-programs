package sql;

import java.sql.*;

public class ConnectionApp {

    static void selectAll(Connection conn) throws SQLException {
        String sql =
        """
        SELECT
            id,
            name
        FROM
            users
        """;
        PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            System.out.printf("user: id: %d, name: %s\n", id, name);
        }
    }

    static void insertOne(Connection conn, String  name) throws SQLException {
        String sql =
        """
        INSERT INTO users (
            name
        )
        VALUES (
            ?
        )
        """;
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, name);
        stmt.execute();
    }

    static void updateOne(Connection conn, int id, String  name) throws SQLException {
        String sql =
        """
        UPDATE users
        SET
            name = ?
        WHERE
            id = ?
        """;
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, name);
        stmt.setInt(2, id);
        stmt.execute();
    }

    static void deleteOne(Connection conn, int id) throws SQLException {
        String sql =
        """
        DELETE FROM users
        WHERE
            id = ?
        """;
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.execute();
    }
    public static void main(String[] args) throws SQLException {
        Connection conn = Database.conn();
    }
}
