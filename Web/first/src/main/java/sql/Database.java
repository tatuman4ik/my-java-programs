package sql;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private static final String url = "jdbc:postgresql://localhost:5432/postgres";
    private static final String user = "pgadmin";
    private static final String password = "pgadmin";

    public static void checkAndApplyDeltas() {
        FluentConfiguration fc = new FluentConfiguration().dataSource(url, user, password);
        Flyway flyway = new Flyway(fc);
        flyway.migrate();
    }

    public static Connection conn() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
}
