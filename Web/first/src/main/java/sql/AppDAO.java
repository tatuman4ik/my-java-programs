package sql;

import java.sql.*;

public class AppDAO {

    public static final String[] names = {"Lucy", "John", "Samantha", "Oleg", "Molly", "George"};
    public static final int defaultPageSize = 10;

    public static String name() {
        return names[(int) (Math.random() * names.length)];
    }

    record Page(int number) {
        public String toSQL() {
            return String.format(
                    """
                            OFFSET %d
                            LIMIT %d
                            """, (number - 1) * defaultPageSize, defaultPageSize
            );
        }
    }
    public static void main(String[] args) throws SQLException {
        Connection conn = Database.conn();

        DAO<User> userDAO = new UserDao(conn);

        Page page3 = new Page(3);
        System.out.println(page3.toSQL());

//        IntStream.range(1, 100).forEach(x -> {
//            try {
//                userDAO.save(new User(null, name()));
//            } catch (SQLException e) {
//                throw new RuntimeException(e);
//            }
//        });
    }
}
