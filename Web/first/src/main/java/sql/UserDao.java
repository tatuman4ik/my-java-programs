package sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserDao implements DAO<User>{
    private final Connection conn;

    public UserDao(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void save(User user) throws SQLException {
        if (user.id() == null) insert(user);
        else                   update(user);
    }
    @Override
    public Optional<User> load(int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("SELECT id, name FROM users WHERE id = ?");
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            return Optional.of(new User(
                    rs.getInt("id"),
                    rs.getString("name")
            ));
        }
        return Optional.empty();
    }
    @Override
    public void delete(int id) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("DELETE FROM users WHERE id = ?");
        stmt.setInt(1, id);
        stmt.execute();
    }
    private void insert(User user) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("INSERT INTO users (name) values (?)");
        stmt.setString(1, user.name());
        stmt.execute();
    }
    private void update(User user) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement("UPDATE users SET name = ? WHERE id = ?");
        stmt.setString(1, user.name());
        stmt.setInt(2, user.id());
        stmt.execute();
    }

}
