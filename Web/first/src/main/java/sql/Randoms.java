package sql;

import java.util.Arrays;
import java.util.Random;

public class Randoms {
    public static void main(String[] args) {
        int[] randoms = (new Random()).ints(10, 90).limit(100).toArray();
        Arrays.sort(randoms);
        System.out.println(Arrays.toString(randoms));
    }
}
