package testLauncher;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class HtmlServlet extends HttpServlet {

    public HtmlServlet() {
        super();
        System.out.println("Hello there");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String uri = Objects.requireNonNull(getClass()
                        .getClassLoader()
                        .getResource("page.html"))
                .getFile();
        Path fileWithFullPath = Paths.get(uri);

        try (PrintWriter w = resp.getWriter()) {
            Files
                    .readAllLines(fileWithFullPath)
                    .forEach(w::println);
        }
    }
}
