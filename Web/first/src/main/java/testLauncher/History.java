package testLauncher;

import java.util.Optional;
import java.util.UUID;

public interface History {
    void put(Item item, UUID id);
    Iterable<Item> getAll(UUID id);
}
