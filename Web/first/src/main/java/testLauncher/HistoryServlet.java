package testLauncher;

import sql.Auth;

import javax.servlet.http.*;
import java.io.*;
import java.util.UUID;

public class HistoryServlet extends HttpServlet {
    private final History history;

    public HistoryServlet(History history) {
        this.history = history;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UUID uid = Auth.getCookieOrThrow(req);

        try (PrintWriter w = resp.getWriter()) {
            history.getAll(uid).forEach(w::println);
        }
    }
}
