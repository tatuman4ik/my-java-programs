package testLauncher;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class StaticContentServlet extends HttpServlet {
    private final String osStaticLocation;

    public StaticContentServlet() {
        this.osStaticLocation = locationDir();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String pathInfo = req.getPathInfo();
        if (pathInfo.startsWith("/")) pathInfo = pathInfo.substring(1);
        Path file = Path.of(osStaticLocation, pathInfo);
        try (ServletOutputStream os = resp.getOutputStream()) {
            Files.copy(file, os);
        }
    }

    private String locationDir() {
        return Paths.get(
                        Objects.requireNonNull(getClass()
                                        .getClassLoader()
                                        .getResource("static-content"))
                                .getPath()
                )
                .toString();
    }
}
