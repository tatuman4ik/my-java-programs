package testLauncher;

import javax.servlet.ServletException;
import java.io.IOException;

public interface ConsumerEx<A> {
    void accept(A a) throws ServletException, IOException;
}
