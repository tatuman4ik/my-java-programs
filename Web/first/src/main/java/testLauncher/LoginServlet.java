package testLauncher;

import freemarker.template.*;
import sql.Auth;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
        cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.tempDirUnsafe("form.ftl")));

        try (PrintWriter w = resp.getWriter()) {
            cfg
                    .getTemplate("form.ftl")
                    .process(new HashMap<String, Object>(), w);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        Optional<UUID> uuidOptional = Auth.getUUIDFromDatabase(username, password);

        if (uuidOptional.isPresent()) {
            UUID uuid = uuidOptional.get();
            Cookie cookie = new Cookie("UID", uuid.toString());
            resp.addCookie(cookie);
            resp.sendRedirect(req.getContextPath() + "/home");
        } else {
            Cookie c = new Cookie("UID", "");
            c.setMaxAge(0);
            resp.addCookie(c);
            resp.sendRedirect(req.getContextPath() + "/login");
        }
    }

}
