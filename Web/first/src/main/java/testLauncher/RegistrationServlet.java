package testLauncher;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import sql.Auth;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

public class RegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
        cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.tempDirUnsafe("form.ftl")));

        try (PrintWriter w = resp.getWriter()) {
            cfg
                    .getTemplate("form.ftl")
                    .process(new HashMap<String, Object>(), w);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username.isEmpty() || password.isEmpty()) resp.sendRedirect(req.getContextPath() + "/reg");

        Optional<UUID> uuidOptional = Auth.getUUIDFromDatabase(username, password);

        if (!uuidOptional.isPresent()) Auth.registerUser(username, password);
        resp.sendRedirect(req.getContextPath() + "/login");
    }

}
