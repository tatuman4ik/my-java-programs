package testLauncher;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import sql.Auth;
import sql.Database;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import java.sql.Connection;
import java.util.EnumSet;
import java.util.Optional;

public class Main {
    public static void main(String[] args) throws Exception {
        Integer port = Optional.ofNullable(System.getenv("PORT"))
                .flatMap(s -> {
                    try {
                        return Optional.of(Integer.parseInt(s));
                    } catch (Exception x) {
                        return Optional.empty();
                    }
                }).orElse(8080);

        Server server = new Server(port);

        Database.checkAndApplyDeltas();
        Connection conn = Database.conn();
        History history =
                new HistoryInDb(conn);
//                new HistoryInMemory();

        ServletContextHandler handler = new ServletContextHandler();

        EnumSet<DispatcherType> dt = EnumSet.of(DispatcherType.REQUEST);

        HttpFilter filter = HttpFilter.make(
                rq -> Auth.tryGetCookie(rq).isPresent(),
                rs -> rs.sendRedirect("https://google.com")
        );
//        handler.addFilter(MyFilter.class, "/history", EnumSet.of(DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(filter), "/history", dt);
        handler.addServlet(new ServletHolder(new HistoryServlet(history)), "/history");

        handler.addServlet(new ServletHolder(new HtmlServlet()), "/html");
        handler.addServlet(new ServletHolder(new TemplateServlet()), "/temp");
        handler.addServlet(new ServletHolder(new HandleFormServlet()), "/form");
        handler.addServlet(new ServletHolder(new BinPicServlet()), "/bin");

        Filter checkParamFilter = HttpFilter.make(
                rq -> rq.getParameter("x") != null && rq.getParameter("y") != null,
                rs -> rs.setStatus(400)
        );

        Filter loggerFilter = HttpFilter.prefix(
                rq -> System.out.println(rq.getRequestURL())
        );

        handler.addFilter(new FilterHolder(checkParamFilter), "/calc", dt);
        handler.addFilter(new FilterHolder(filter), "/calc", dt);
        handler.addFilter(new FilterHolder(loggerFilter), "/calc", dt);
        handler.addServlet(new ServletHolder(new CalculatorServlet(history)), "/calc");
        handler.addServlet(new ServletHolder(new CalculatorMapServlet()), "/calcmap");
        handler.addServlet(new ServletHolder(new CalculatorMapAServlet()), "/calcmapa");
        handler.addServlet(new ServletHolder(new CalculatorMapASplitServlet()), "/calcmapasplit");


        handler.addServlet(new ServletHolder(new LoginServlet()), "/login");
        handler.addServlet(new ServletHolder(new RegistrationServlet()), "/reg");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");
        handler.addServlet(new ServletHolder(new HomeServlet()), "/home");

        handler.addServlet(new ServletHolder(new FileDownloadServlet()), "/download");
        ServletHolder sh = new ServletHolder(new FileUploadServlet());
        sh.getRegistration().setMultipartConfig(new MultipartConfigElement(".//from-user"));
        handler.addServlet(sh, "/upload");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}
