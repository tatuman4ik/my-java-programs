package testLauncher;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Objects;

public class FileUploadServlet extends HttpServlet {

    public FileUploadServlet() {
        super();
        System.out.println("Hello file upload");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
        cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.tempDirUnsafe("upload.ftl")));

        try (PrintWriter w = resp.getWriter()) {
            cfg
                    .getTemplate("upload.ftl")
                    .process(new HashMap<String, Object>(), w);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (PrintWriter w = resp.getWriter()) {
            for (Part part: req.getParts()) {
                System.out.println(part.getSubmittedFileName());
                byte[] bytes = part.getInputStream().readAllBytes();
                String s = new String(bytes);
                w.println(s);
                w.println();
        }

        }
    }
}
