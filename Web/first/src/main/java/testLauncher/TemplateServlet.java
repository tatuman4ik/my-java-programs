package testLauncher;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class TemplateServlet extends HttpServlet {

    public TemplateServlet() {
        super();
        System.out.println("Hello template");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
        cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.tempDirUnsafe("template.ftl")));

        HashMap<String, Object> table = new HashMap<>();
        table.put("name", "JIM");
        ArrayList<PriceLine> items = new ArrayList<>();
        items.add(new PriceLine(1, "Iphone 5", 200.5));
        items.add(new PriceLine(2, "Iphone 10", 550.5));
        items.add(new PriceLine(3, "Iphone 13Pro", 1050.5));
        table.put("items", items);
        try (PrintWriter w = resp.getWriter()) {
            cfg
                    .getTemplate("template.ftl")
                    .process(table, w);
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }
}
