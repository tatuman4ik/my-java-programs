package testLauncher;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public record Item(int x, int y, int z, Timestamp ts) {

     public static Item of (int x, int y, int z) {
        return new Item(x, y, z, new Timestamp(System.currentTimeMillis()));
    }

    public static Item of (int x, int y, int z, Timestamp ts) {
        return new Item(x, y, z, ts);
    }

    @Override
    public String toString() {
        return (ts == null)
        ? String.format("%d + %d = %d, at = null", x, y, z)
        : String.format("%d + %d = %d, at = %-30s", x, y, z, ts.toString().split("\\.")[0]);
    }
}