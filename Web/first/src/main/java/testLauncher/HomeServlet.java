package testLauncher;

import javax.servlet.http.*;
import java.io.IOException;

public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Cookie[] cookies = req.getCookies();
        boolean isAuthenticated = false;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("UID")) {
                    isAuthenticated = true;
                    break;
                }
            }
        }
        if (isAuthenticated) {
            resp.getWriter().println("Welcome!");
        } else {
            resp.sendRedirect(req.getContextPath() + "/login");
        }
    }
}
