package testLauncher;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

public class CalculatorMapServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, String[]> allParameters = req.getParameterMap();
        StringBuilder sb = new StringBuilder();
        int z = 0;
        for (String key: allParameters.keySet()) {
            String p = allParameters.get(key)[0];
            if (!sb.isEmpty()) sb.append(" + ");
            sb.append(p);
            z += Integer.parseInt(p);
        }
        sb.append(String.format(" = %d", z));

        try (Writer w = resp.getWriter()) {
            w.write(sb.toString());
        }
    }
}
