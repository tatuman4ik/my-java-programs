package testLauncher;

import sql.DbUtils;

import java.sql.*;
import java.util.Optional;
import java.util.UUID;

public class HistoryInDb implements History{

    private final Connection conn;

    public HistoryInDb(Connection conn) {
        this.conn = conn;
    }

    private void saveToDb(Item item, UUID id) throws SQLException {
        PreparedStatement st = conn.prepareStatement(
                """
                        INSERT INTO history (x, y, z, t) VALUES (?,?,?,?)
                        """
        );
        st.setInt(1, item.x());
        st.setInt(2, item.y());
        st.setInt(3, item.z());
        st.setObject(4, item.ts());
        st.execute();
    }

    public Iterable<Item> getAllFromDb() throws Exception {
        PreparedStatement st = conn.prepareStatement(
            """
                    SELECT * FROM history ORDER BY id
                    """
    );
        ResultSet rs0 = st.executeQuery();
        return DbUtils.convert(rs0, rs -> {
            int x = rs.getInt("x");
            int y = rs.getInt("y");
            int z = rs.getInt("z");
            Timestamp ts = rs.getTimestamp("t");
            return Item.of(x, y, z, ts);
        });
    }

    @Override
    public void put(Item item, UUID id) {
        try {
            saveToDb(item, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Iterable<Item> getAll(UUID id) {
        try {
            return getAllFromDb();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
