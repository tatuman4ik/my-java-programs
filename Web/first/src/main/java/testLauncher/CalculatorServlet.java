package testLauncher;

import sql.Auth;

import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class CalculatorServlet extends HttpServlet {
    private final History history;

    public CalculatorServlet(History history) {
        this.history = history;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UUID uid = Auth.getCookieOrThrow(req);
        String xs = req.getParameter("x");
        String ys = req.getParameter("y");

        int x = Integer.parseInt(xs);
        int y = Integer.parseInt(ys);
        int z = x + y;
        history.put(Item.of(x, y, z), uid);

        try (Writer w = resp.getWriter()) {
             w.write(String.format("%d + %d = %d", x, y, z));
        }
    }
}
