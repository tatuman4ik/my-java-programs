package testLauncher;

import java.util.*;

public class HistoryInMemory implements History{
    private final Map<UUID, LinkedList<Item>> storage = new HashMap<>();

    @Override
    public void put(Item item, UUID id) {
        if (!storage.containsKey(id)) {
            storage.put(id, new LinkedList<>());
        }
        storage.get(id).add(item);
    }

    @Override
    public Iterable<Item> getAll(UUID id) {
        LinkedList<Item> items = storage.get(id);
        return items != null ? items : Collections.emptyList();
    }
}
