package testLauncher;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class BinPicServlet extends HttpServlet {

    public BinPicServlet() {
        super();
        System.out.println("Hello picture");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String uri = Objects.requireNonNull(getClass()
                        .getClassLoader()
                        .getResource("static-content/img/pic.jpg"))
                .getFile();
        System.out.println(uri);
        Path fileWithFullPath = Paths.get(uri);
        try (ServletOutputStream os = resp.getOutputStream()) {
            Files.copy(fileWithFullPath, os);
        }
    }
}