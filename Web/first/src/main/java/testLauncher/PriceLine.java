package testLauncher;

public class PriceLine {
    public Integer id;
    public String name;
    public Double price;

    public PriceLine(Integer id, String name, Double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }
}
