package testLauncher;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

public class CalculatorMapASplitServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, String[]> allParameters = req.getParameterMap();
        String[] as = allParameters.get("a")[0].split(",");
        StringBuilder sb = new StringBuilder();
        int z = 0;
        for (String a: as) {
            if (!sb.isEmpty()) sb.append(" + ");
            sb.append(a);
            z += Integer.parseInt(a);
        }
        sb.append(String.format(" = %d", z));

        try (Writer w = resp.getWriter()) {
            w.write(sb.toString());
        }
    }
}
