package testLauncher;

import java.nio.file.Paths;
import java.util.Objects;

public class ResourcesOps {

    public static String tempDirUnsafe(String fn) {
        return Paths.get(
                        ResourcesOps.class
                                        .getClassLoader()
                                        .getResource(fn)
                                .getPath()
                )
                .getParent()
                .toString();
    }
}
