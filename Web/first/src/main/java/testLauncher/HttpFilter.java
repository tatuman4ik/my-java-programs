package testLauncher;

import sql.Auth;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Function;

public interface HttpFilter extends Filter {
    @Override
    default void init(FilterConfig filterConfig) {}

    private boolean isHttp(ServletRequest servletRequest, ServletResponse servletResponse) {
        return servletRequest instanceof HttpServletRequest && servletResponse instanceof HttpServletResponse;
    }

    void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException;

    @Override
    default void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (isHttp(servletRequest, servletResponse)) {
            doHttpFilter((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, filterChain);
        } else filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    default void destroy() {}

    static HttpFilter prefix(ConsumerEx<HttpServletRequest> preFn) {
        return new HttpFilter() {
            @Override
            public void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
                preFn.accept(request);
                filterChain.doFilter(request, response);
            }
        };
    }

    static HttpFilter make(
            Function<HttpServletRequest, Boolean> checkFn,
            ConsumerEx<HttpServletResponse> failedFn) {
        return new HttpFilter() {
            @Override
            public void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
                if (checkFn.apply(request)) {
                    filterChain.doFilter(request, response);
                } else failedFn.accept(response);
            }
        };
    }
}
