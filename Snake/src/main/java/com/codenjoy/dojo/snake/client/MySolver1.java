package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.Solver;

public class MySolver1 implements Solver<Board> {

    @Override
    public String get(Board board) {
        System.out.println(board.toString());
        RouteFinder finder = new RouteFinder(board);
        return finder.checkedDirection().toString();
        }

}
