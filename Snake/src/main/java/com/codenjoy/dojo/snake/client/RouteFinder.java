package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class RouteFinder {
    private final Board board;
    private Cell[][] cells;
    int counter = 0;

    public RouteFinder(Board board) {
        this.board = board;
        this.cells = setCells();
        setAim(board.getApples().get(0));
        setEnemy(board.getStones().get(0));
        setWalls(board.getWalls());
        setSnake(board.getSnake());
    }

    private Cell[][] setCells() {
        Cell[][] newCells = new Cell[15][15];
        for (int row = 0; row < 15; row++)
            for (int column = 0; column < 15; column++)
                newCells[row][column] = new Cell(row, column);
        return newCells;
    }

    private int sX(Point point) {
        return point.getY();
    }

    private int sY(Point point) {
        return 15 - point.getX() - 1;
    }

    private void setAim(Point point) {
        cells[sX(point)][sY(point)].setAim();
    }

    private void setEnemy(Point point) {
        cells[sX(point)][sY(point)].setEnemy();
    }

    private void setWalls(List<Point> point) {
        point.forEach(a -> cells[sX(a)][sY(a)].setWall());
    }

    private void setSnake(List<Point> point) {
        if (!point.isEmpty()) {
            point.forEach(a -> cells[sX(a)][sY(a)].setSnakeBody());
            cells[sX(point.get(0))][sY(point.get(0))].setHead();
        }
    }

    private void setValue(Point point, int value) {
        cells[sX(point)][sY(point)].setStep(value);
        cells[sX(point)][sY(point)].setVisited();
    }

    private Cell getCell(Point point) {
        return cells[sX(point)][sY(point)];
    }

    public Direction checkedDirection() {
        return checkedDirection(direction());
    }

    public Direction checkedDirection(Direction dir) {
        return board.getHead() != null && getCell(dir.change(board.getHead())).isSafe()
                ? dir
                : checkedDirection(dir.clockwise());
    }

    public Direction direction() {
        if (board.getSnake().isEmpty()) return Direction.RIGHT;
        getCell(board.getTail()).setFree();
        Optional<Direction> maybe = changeDir(Set.of(new Point[]{board.getHead()}));
        if (maybe.isPresent()) return maybe.get();
        this.cells = setCells();
        setAim(board.getStones().get(0));
        setEnemy(board.getApples().get(0));
        setWalls(board.getWalls());
        setSnake(board.getSnake());
        this.counter = 0;
        maybe = changeDir(Set.of(new Point[]{board.getHead()}));
        return maybe.orElse(Direction.RIGHT);
    }

    private Optional<Direction> changeDir(Set<Point> start) {
        this.counter++;
        Set<Point> maybeNext =
                Direction.getValues()
                        .stream()
                        .flatMap(dir -> start.stream()
                                        .map(dir::change)
                                        .filter(
                                                a -> !getCell(a).isVisited
                                                && getCell(a).isSafe()
                                        ))
                        .collect(Collectors.toSet());
        if (maybeNext.isEmpty()) return Optional.empty();
        maybeNext.forEach(b -> setValue(b, this.counter));
        Optional<Point> aim = findAim(maybeNext);
        if (aim.isPresent()) {
            System.out.println(aim.get());
            return findFirstMove(aim.get(), this.counter);
        }
        return changeDir(maybeNext);
    }

    private Optional<Point> findAim(Set<Point> points) {
        for(Point p : points) {
            if (getCell(p).isAim) {
                return Optional.of(p);
            }
        }
        return Optional.empty();
    }

    private Optional<Direction> findFirstMove(Point point, int steps) {
        if (steps == 1) return checkDir(point);
        return findFirstMove(
                Direction.getValues()
                        .stream()
                        .map(dir -> dir.change(point))
                        .filter(p -> getCell(p).step == steps - 1 && !getCell(p).isSnakeBody)
                        .findFirst().get(),
                steps - 1);
    }

    private Optional<Direction> checkDir(Point next) {
        Optional<Direction> first = Direction.getValues().stream()
                        .filter(
                                dir -> getCell(dir.change(next)).isHead
                        )
                        .findFirst();
        return first.map(Direction::inverted);
    }

}
