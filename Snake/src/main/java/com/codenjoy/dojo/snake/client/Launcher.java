package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.WebSocketRunner;

public class Launcher {

    public static void main(String[] args) {
        String url = "http://64.226.126.93/codenjoy-contest/board/player/20we5jtumymky57e04l7?code=4046848160848696009";
        MySolver1 mySolver = new MySolver1();
        Board board = new Board();

        WebSocketRunner.runClient(url, mySolver, board);
    }

}
