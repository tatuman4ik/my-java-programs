package com.codenjoy.dojo.snake.client;

public class Cell {
    int x;
    int y;
    boolean isVisited;
    boolean isAim;
    boolean isEnemy;
    boolean isHead;
    boolean isSnakeBody;
    boolean isWall;


    public void setWall() {
        isWall = true;
    }
    int step = 0;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setStep(int step) {
        this.step = step;
        this.isVisited = true;
    }

    public void setVisited() {
        isVisited = true;
    }

    public void setAim() {
        this.setFree();
        this.isAim = true;
    }

    public void setEnemy() {
        this.setFree();
        this.isEnemy = true;
    }

    public void setHead() {
        isHead = true;
    }

    public void setSnakeBody() {
        isSnakeBody = true;
    }

    public boolean isSafe() {
        return !(isWall || isEnemy || isSnakeBody);
    }

    public void setFree() {
        this.isEnemy = false;
        this.isSnakeBody = false;
        this.isVisited = false;
        this.isAim = false;
        this.isHead = false;
        this.step = 0;
    }

}
