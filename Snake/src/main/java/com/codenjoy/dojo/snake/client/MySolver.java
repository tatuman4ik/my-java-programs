package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;

import java.util.LinkedList;
import java.util.List;

public class MySolver implements Solver<Board> {
    boolean start;
    List<Point> boardEdge;
    List<Point> snakeBody;
    Point head, aim, enemy;
    Direction currentDir;

    private void setAim(Point aim, Point enemy) {
        this.aim = aim;
        this.enemy = enemy;
    }

    private Direction changeDir(Board board) {
        if (!start) {
            start = true;
            boardEdge = board.getWalls();
        }

        snakeBody = board.getSnake();
        head = snakeBody.get(0);
        currentDir = board.getSnakeDirection();

        if (snakeBody.size() < 50) setAim(board.getApples().get(0), board.getStones().get(0));
        else setAim(board.getStones().get(0), board.getApples().get(0));

        return checkSafeDirs(head, possibleDir(head));
    }

    private Direction possibleDir(Point current) {
        if (aim.getX() < current.getX())
            if (isSafeMove(current, Direction.LEFT)) return Direction.LEFT;
        if (aim.getY() > current.getY())
            if (isSafeMove(current, Direction.UP)) return Direction.UP;
        if (aim.getX() > current.getX())
            if (isSafeMove(current, Direction.RIGHT)) return Direction.RIGHT;
        if (aim.getY() < current.getY())
            if (isSafeMove(current, Direction.DOWN)) return Direction.DOWN;
        return currentDir;
    }

    private Direction checkSafeDirs(Point current, Direction direction) {
        if (isSuccess(current, direction)) return direction;

        LinkedList<Direction> directions = new LinkedList<>();
        if (isSafeMove(current, direction)) directions.add(direction);

        Direction.getValues()
                .stream()
                .filter(
                        x -> isSafeMove(current, x)
                )
                .forEach(directions::add);
        return directions.size() != 0 ? directions.getFirst() : currentDir.inverted();
    }

    private boolean isSuccess(Point current, Direction moveTo) {
        return (aim.equals(moveTo.change(current)));
    }

    private boolean isSafeMove(Point current, Direction moveTo) {
        return !(snakeBody.contains(moveTo.change(current))
                || boardEdge.contains(moveTo.change(current))
                || enemy.equals(moveTo.change(current)));
    }

    @Override
    public String get(Board board) {
        System.out.println(board.toString());
        return changeDir(board).toString();
        }

}
