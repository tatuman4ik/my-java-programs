import java.util.Scanner; // импорт сканера
class Test {
    public static float check(String enter){ //проверка введенных данных
        boolean entNum;
        do {
            Scanner scan = new Scanner(System.in);
            System.out.println(enter);
            if(scan.hasNextFloat()){ //проверка на число
                entNum=true;
                float itCanBeRight=scan.nextFloat();
                if(itCanBeRight>0){ //проверка на отрицательность
                    return (itCanBeRight);
                } else {
                    System.out.println("It can be only more than 0");
                    entNum=false;
                }
            } else {
                System.out.println("Only numbers please");
                entNum=false;
            }
        }while (true);
    }
    public static void main(String[] args){
        float priceBTC = check("What is Bitcoin price today?");
        float availableMoney = check("How much $ do you have?");
        float availableBTC = (availableMoney/priceBTC);
        System.out.printf("You can buy %.7f BTC",availableBTC);
    }
}