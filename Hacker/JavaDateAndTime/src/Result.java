import java.util.Calendar;
import java.util.Locale;
class Result {
    /*
     * Complete the 'findDay' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. INTEGER month
     *  2. INTEGER day
     *  3. INTEGER year
     */
    public static String findDay(int month, int day, int year) {
        Calendar past = Calendar.getInstance();
        past.set(year, month - 1, day);
        String answer = past.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        return answer.toUpperCase();
    }
}
