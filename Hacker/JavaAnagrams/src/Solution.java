import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {

    static boolean isAnagram(String a, String b) {
        // Complete the function
        return resultMap(a).equals(resultMap(b));
    }

    static Map<Character, Integer> resultMap(String x) {
        Map<Character, Integer> result = new HashMap<>();
        for (char letter : x.toLowerCase().toCharArray()) {
            if (result.containsKey(letter)) result.put(letter, result.get(letter) + 1);
            else result.put(letter, 1);
        }
        return result;
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
    }
}