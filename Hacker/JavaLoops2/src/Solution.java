import java.util.*;
import java.io.*;

class Solution{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            int res = a;
            int marker = 1;
            for (int j = 0; j < n; j++) {
                res += marker * b;
                System.out.print(res);
                if (j + 1 < n) {
                    System.out.print(' ');
                    marker *= 2;
                } else System.out.println();
            }
        }
        in.close();
    }
}