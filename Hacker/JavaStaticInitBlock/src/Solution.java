import javax.swing.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
//    Write your code here
    static int B, H;
    static boolean flag;
    static Scanner sc = new Scanner(System.in);
    static {
        B = sc.nextInt();
        H = sc.nextInt();
        flag = (B > 0 && H > 0) ? true : false;
        if (!flag) System.out.println("java.lang.Exception: Breadth and height must be positive");
    }
    public static void main(String[] args){
        if(flag){
            int area=B*H;
            System.out.print(area);
        }

    }//end of main

}//end of class

