public class Solution {

    public static boolean isAddSafe(long x, long y) {
//        return (x >> 63 != y >> 63) || ((x >> 63 | y >> 63) == (x + y) >> 63);
        return (x >>> 63 ^ y >>> 63) > 0 || (x >>> 63 ^ (x + y) >>> 63) < 1;
    }
    public static void main(String[] args) {
        System.out.println(isAddSafe(Long.MAX_VALUE, 100));
        System.out.println(isAddSafe(Long.MIN_VALUE, -100));
        System.out.println(isAddSafe(Long.MAX_VALUE, 0));
        System.out.println(isAddSafe(Long.MIN_VALUE, 0));
        System.out.println(isAddSafe(Long.MAX_VALUE, -1));
        System.out.println(isAddSafe(Long.MIN_VALUE, 1));
        System.out.println(isAddSafe(-2, 3));
        System.out.println(isAddSafe(-3, 2));
        System.out.println(isAddSafe(-2, -3));
        System.out.println(isAddSafe(-3, -2));
    }
}
