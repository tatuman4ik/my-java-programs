import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String A=sc.next();
        /* Enter your code here. Print output to STDOUT. */
        int i = 0;
        String answer = "Yes";
        do {
            if (!(A.substring(i, i+1)
                    .equals(
                            A.substring(A.length()-i-1,
                                    A.length()-i)
                    )))
                answer = "No";
            i++;
        } while (i < A.length()/2 && answer.equals("Yes"));
        System.out.println(answer);
    }
}