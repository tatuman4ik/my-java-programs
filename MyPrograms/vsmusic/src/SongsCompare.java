import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class SongsCompare {
    public static void main(String[] args) {
        // Считываем список из файла
        List<String> items = readItemsFromFile("/media/tatuman/0630732803/Программирование/JAVA/my-java-programs/MyPrograms/vsmusic/src/list.txt");

        // Перемешиваем список
        Collections.shuffle(items);

        // Создаем список победителей
        List<String> winners = new ArrayList<>(items);

        // Проводим турнир
        while (winners.size() > 1) {
            List<String> roundWinners = new ArrayList<>();
            for (int i = 0; i < winners.size(); i += 2) {
                if (i + 1 < winners.size()) {
                    String item1 = winners.get(i);
                    String item2 = winners.get(i + 1);
                    String winner = playRound(item1, item2);
                    if (winner != null) {
                        roundWinners.add(winner);
                    }
                }
            }
            winners = roundWinners;
        }

        // Выводим финального победителя
        System.out.println("Победитель: " + winners.get(0));
    }

    private static List<String> readItemsFromFile(String fileName) {
        List<String> items = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                String item = scanner.nextLine().trim();
                if (!item.isEmpty()) {
                    items.add(item);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Не удалось прочитать файл " + fileName);
        }
        return items;
    }

    private static String playRound(String item1, String item2) {
        System.out.println("Выберите победителя:");
        System.out.println("1. " + item1);
        if (item2 != null) {
            System.out.println("2. " + item2);
        }
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            if (input.equals("1")) {
                return item1;
            } else if (input.equals("2")) {
                return item2;
            } else {
                System.out.println("Неверный ввод, попробуйте еще раз");
            }
        }
    }
}
