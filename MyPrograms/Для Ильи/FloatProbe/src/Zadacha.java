import java.util.Scanner;
public class Zadacha{
    /*Ввести дійсне число number і отримати
     2 перші цифри після коми цього числа.
     Вивести суму цих цифр. Напр.: 3.456->4+5=9
     */
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println ("Введіть дійсне число");
 /*       float floatNum = scan.nextFloat();
        int number1 = (int) (floatNum * 10) - ((int) floatNum) * 10;
        int number2 = (int) (floatNum * 100) - ((int) (floatNum * 10)) * 10;
        System.out.println (number1+number2);

 */
        float num = scan.nextFloat();
        int x = (int) (num * 100) - ((int) num) * 100;
        int sum = x/10 + x%10;
        System.out.println (sum);
    }
}