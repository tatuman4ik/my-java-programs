import java.util.Scanner;
public class Zadacha{
        /*Ввести дійсне число number і отримати
         2 перші цифри після коми цього числа.
         Вивести суму цих цифр. Напр.: 3.456->4+5=9
         */
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println ("Введіть дійсне число");
        float floatNum = scan.nextFloat();
        int sum = (int) floatNum * 10 - ((int) floatNum) * 10;
        sum+= (int) floatNum * 100 - ((int) floatNum) * 100;
        System.out.println (sum);
    }
}