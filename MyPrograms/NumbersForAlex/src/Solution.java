import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution {
    public static Map<String,Long> countLetters(int min, int max) {
        String input = IntStream.rangeClosed(min, max)// здесь последовательность интов пишется в строку (173, 174, 175,,,)
                .mapToObj(String::valueOf)//здесь инты становятся строками ("173", "174", "175",,,)
                .collect(Collectors.joining());//здесь строки склеиваются в одну строку (173174175...)
        return input.codePoints()//здесь строка разбирается по символам (1, 7, 3, 1, 7, 4, 1, 7, 5,,,)
                .mapToObj(c -> String.format("'%c'", c))
                .collect(Collectors.groupingBy(
                c -> c,
                Collectors.counting()
        ));
    }
    public static void main(String[] args) {
        Map<String, Long> characterIntegerMap = countLetters(173, 215);
        System.out.println(characterIntegerMap);
    }
}
