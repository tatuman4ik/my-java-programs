import java.util.*;
/* t.me/testforgoitelsebot
Use this token to access the HTTP API:
5557728810:AAGs6RM3-8uGUsePaWYiTvINLFBeA23sB4Y
 */
public class ChatBot {
    private static Map<String, Integer> professions;
    private static Map<String, Integer> dreams;

    public static void main(String[] args) {
        System.out.println ("Вітаю, я - GoITeensChatBot");
        boolean success = false;
        do {
            System.out.println("Напиши мені ким ти працюєш і про що мрієш");
            Scanner scanner = new Scanner(System.in);
            String message = scanner.nextLine();

            initProfessions();
            initDreams();

            int botAnswer = process(message);
            if (botAnswer == -1) {
                System.out.println("Я не знайшов у твоєму повідомленні назви професії");
            }
            if (botAnswer == 0) {
                System.out.println("Я не знайшов у твоєму повідомленні мрії, яку ти хочеш");
            }
            if (botAnswer > 0) {
                System.out.println("Щоб купити свою мрію, тобі потрібно працювати протягом " + botAnswer + " місяців");
                System.out.println("Бажано при цьому ще й не витрачати гроші ;)");
                success = true;
                System.out.println("Для ще однієї спроби напиши 'Ще'");
                String messageToRestart = scanner.nextLine();
                messageToRestart = messageToRestart.toLowerCase();
                if (messageToRestart.contains("ще")) {
                    success = false;
                }
            }
        }while (!success);
    }

    private static void initProfessions() {
        professions = new LinkedHashMap<>();

        professions.put("Designer", 20000);
        professions.put("Java", 55000);
        professions.put("Frontend", 40000);
    }

    private static void initDreams() {
        dreams = new LinkedHashMap<>();

        dreams.put("Машин", 260000);
        dreams.put("iPhone", 27000);
    }

    public static int process(String message) {
        int professionSalary = find(message, professions);
        int dreamCost = find(message, dreams);

        if (professionSalary < 0) {
            return -1;
        }

        if (dreamCost < 0) {
            return 0;
        }

        return calculateMonthCount(dreamCost, professionSalary);
    }

    public static int find(String message, Map<String, Integer> data) {
        message = message.toLowerCase();

        for(String word: data.keySet()) {
            String lowerCasedWord = word.toLowerCase();

            if (message.contains(lowerCasedWord)) {
                return data.get(word);
            }
        }

        return -1;
    }

    public static int calculateMonthCount(int dreamCost, int professionSalary) {
        int monthCount = dreamCost / professionSalary;
        monthCount = validateMonthCount(monthCount);
        return monthCount;
    }

    public static int validateMonthCount(int monthCount) {
        if (monthCount == 0) {
            return 1;
        }
        return monthCount;
    }
}